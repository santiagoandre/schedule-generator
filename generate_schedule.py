import json
import array
import random
import math
import sys
import numpy as np

import multiprocessing
from deap import algorithms
from deap import base
from deap import creator
from deap import tools

#LOAD DATA: 
#1) subjects
#with open("processed_data/subjects.json","r",encoding="iso-8859-14") as tsp_data:
#        SUBJECTS = json.load(tsp_data)

#2) teachers
#with open("processed_data/teachers.json","r",encoding="iso-8859-14") as tsp_data:
#        TEACHERS = json.load(tsp_data)
        
#3) groups
with open("processed_data/groups.json","r",encoding="iso-8859-14") as tsp_data:
        GROUPS = json.load(tsp_data)
#4) classrooms
with open("processed_data/classrooms.json","r",encoding="utf-8-sig") as tsp_data:
        CLASSROOMS = json.load(tsp_data)

#5) jsons_size
with open("processed_data/jsons_size.json","r",encoding="utf-8-sig") as tsp_data:
        JSONS_SIZE = json.load(tsp_data)
#DEFINICION DE HORARIO
#        L    M    Mi    J    V
#7-9     0    5    11    17   24
#9-11    1    6    12    18   25
#11-1    2    7    13    19   26        <=== FRANJAS HORARIAS
#2-4     3    8    14    20   27             NTZ = 30 NUMERO DE FRANJAS
#4-6     4    9    15    21   28             DAY = |NTZi/5|
#6-8     5    10   16    22   29
JSONS_SIZE["NTZ"] = 30
#print(SUBJECTS)
#print(TEACHERS)
#print(GROUPS)
#print(CLASSROOMS)
print(JSONS_SIZE)
def configureAlgorithm(toolbox,IND_SIZE,avaluation_f):
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMin)
    # Attribute generator
    toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)
    # Structure initializers
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
    #population: genercion aleatoria
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    #population: genercion definida: le tengo que pasar el constructor de individuos que acabe de definir
    #toolbox.register("population",load_individuals, creator.Individual)
    
    # functions 
    toolbox.register("evaluate", avaluation_f)
    toolbox.register("mate", cxTwoPointSchedule)
    toolbox.register("mutate", mutSchedule,  tzpb = 0.01 ,cpb = 0 )
    toolbox.register("select", tools.selTournament, tournsize=3)

def load_individuals(creator,n):
    individuals = []
    for i in range(n):
        with open("individuals/10000-{0}.txt".format(i),"r") as tsp_data:
            individual = eval(tsp_data.readline())
        individual = creator(individual)
        individuals.append(individual)
    return individuals

def fitSchedule(ind, detailed = False):
    x = iter(ind)
    #cada individuo va reprecentando los grupos en orden
    #recorrer cada grupo
    rule1_bank = [] #materias de un mismo semestre no deben cruzarse
    rule3_bank = [] #un salon no se puede usar por mas de un grupo en una franja horaria
    rule4_bank = [] # un profesor no es omnipresente
    breaches_rules =[0,0,0,0]
    penality_rules =[1.5,1,1.5,1]
    for i,group in enumerate(GROUPS):
        #hay group["BLOCKS"] para el grupo i
        rule2_bank = [] #dos franjas horarias de la misma materia no pueden darse en el mismo dia
        for block in range(group["BLOCKS"]):
          #  print("------- ",group," = ",block , " ---------------")
            TZ = next(x)# siguiente elemento de x, reprecenta  la franga
            C = next(x)# siguiente elemento de x, reprecenta  el salon
            #hay que  arreglar el valor del salon
            C = group["TYPE"] + "-"+ str(C%JSONS_SIZE["NC"+group["TYPE"]])
            TZ = TZ%JSONS_SIZE["NTZ"]
            # el bloque 'block' del grupo 'group' se orienta 
            # en la franga horaria 'F' y en el salon 'S'
            
         #   print("TZ: ", TZ)
         #  print("C: ", C)
            day = int(TZ/6)
            key_rule1 = group["SEMESTER"]+"-"+str(TZ)
            key_rule2 =str(i)+"-"+str(day)
            key_rule3 = C+'-'+str(TZ)
            key_rule4 = group["TEACHER"]+'-'+str(TZ)
            if key_rule1 in rule1_bank:
                breaches_rules[0]+=1
            else:
                rule1_bank.append(key_rule1)
            if key_rule2 in rule2_bank:
                breaches_rules[1]+=1
            else:
                rule2_bank.append(key_rule2)
            if key_rule3 in rule3_bank:
                breaches_rules[2]+=1
            else:
                rule3_bank.append(key_rule3)
            if key_rule4 in rule4_bank:
                breaches_rules[3]+=1
            else:
                rule4_bank.append(key_rule4)
   #     print("RULE2: ",rule2_bank)
#    print(breaches_rules)
#    print("RULE1: ",rule1_bank) 
#    print("RULE3: ",rule3_bank) 
    if detailed:
        return (breaches_rules,sum(breaches_rules))     
    else:
        return (sum(breaches_rules) ,)


def main(iterations = 10):
    block_size =  2 # un numero para TZ(time zone) y otro para C(Salon)
    n_blocks = blocks_count()
    individual_size = n_blocks*(block_size)
    JSONS_SIZE['NBLOCKS'] = n_blocks
    toolbox = base.Toolbox()
    
    configureAlgorithm(toolbox,individual_size,fitSchedule)
    random.seed(23)

    pop = toolbox.population(n=20)
    #print(pop)
    hof = tools.HallOfFame(4)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=0.4   , mutpb=0.25 , ngen=iterations,
                                   stats=stats, halloffame=hof, verbose=True)
    #SE GUARDA EL INDIVIDUO EN BINARIO EN UN ARCHIVO
    for i,hofi in enumerate(hof):
        name_file = "{0}-{1}.txt".format(iterations,i)
        hfile = open("schedules/"+name_file,"w")
        mfile = open("individuals/"+name_file,"w")
        hofi = hofi.tolist()
        mfile.write(str(hofi))
        interprete_individual(hofi,hfile)
        fitness = "fitness: " + str(fitSchedule(hofi,detailed=True)  )
        print(fitness)
        hfile.write(fitness)
        hfile.close()
        mfile.close()
        
    return pop, log, hof



# OPERACIONES CON BINARIOS    

# CALCULAR CARACTERISTICAS DEL INDIVIDUO
def blocks_count():
    #calculate number of gnomes
    n_gnomes = sum( [group["BLOCKS"] for group in GROUPS] )
    return  n_gnomes

            
# INTERPRETAR INDIVIDUO
def interprete_individual(ind,vfile,split="\t"):
    days = ["Lunes","Martes","Miercoles","Jueves","Viernes"]
    blocks = ["7-9","9-11","11-1","2-4","4-6","6-8"]
    x = iter(ind)
    for i,group in enumerate(GROUPS):
        #hay group["BLOCKS"] para el grupo i
        for _ in range(group["BLOCKS"]):
          #  print("------- ",group," = ",block , " ---------------")
            TZ = next(x)# siguiente elemento de x, reprecenta  la franga
            C = next(x)# siguiente elemento de x, reprecenta  el salon
            #hay que  arreglar el valor del salon
            TC = group["TYPE"]
            C = CLASSROOMS[TC][C%JSONS_SIZE["NC"+TC]]
            TZ = TZ%JSONS_SIZE["NTZ"]
            day = days[int(TZ/6)]
            block = blocks[TZ%6]
            #print("el grupo {0} de la materia {1}, se dicta el bloque {2} del dia {3} en el salon {4}".format(group["CODE_GROUP"],group["CODE"],block,day,C))
            # el bloque 'block' del grupo 'group' se orienta 
            # en la franga horaria 'F' y en el salon 'S'
            block_string = group["CODE"] + split+group["CODE_GROUP"] + split + block + split + day + split + str(C) +"\n"
            vfile.write(block_string)
            
         

        
# MUTACION
def mutSchedule(individual, tzpb,cpb):
    
    block_size = 2
    n_blocks =  JSONS_SIZE['NBLOCKS']
    for i in range(n_blocks):
        if random.random() < tzpb:
            time_zone = random.randint(0,JSONS_SIZE["NTZ"]-1)
            individual[i] = time_zone
        if random.random() < cpb:
            classroom = random.randint(0,int(JSONS_SIZE["NC"]-1))
            individual[i+1] = classroom
        
    return individual,
#CRUZE 
def cxTwoPointSchedule(ind1, ind2):
    n_blocks =  JSONS_SIZE['NBLOCKS']
    cxpoint1 = random.randint(1, n_blocks)
    cxpoint2 = random.randint(1, n_blocks - 1)
    if cxpoint2 >= cxpoint1:
        cxpoint2 += 1
    else:  # Swap the two cx points
        cxpoint1, cxpoint2 = cxpoint2, cxpoint1
    ind1[cxpoint1:cxpoint2], ind2[cxpoint1:cxpoint2] \
        = ind2[cxpoint1:cxpoint2], ind1[cxpoint1:cxpoint2]

    return ind1, ind2
def cxOnePointSchedule(ind1, ind2):
    bitsTZ  = JSONS_SIZE['bitsTZ'] 
    bitsC =  JSONS_SIZE['bitsC']
    n_blocks =  JSONS_SIZE['NBLOCKS']
    block_size = bitsTZ + bitsC
    
    cxpoint1 = random.randint(0, n_blocks-1)*block_size
    cxpoint2 = random.randint(0, n_blocks-1)*block_size
    ind1[cxpoint1:], ind2[cxpoint2:] = ind2[cxpoint2:], ind1[cxpoint1:]

    return ind1, ind2


if __name__ == "__main__":
    #test_mutate()
    if len(sys.argv) >=2:
        main(int(sys.argv[1]))
    else:
        main()
def test_mutate():
    block_size = 2
    individual_size = 5*(block_size)
    JSONS_SIZE['NBLOCKS'] = 5
    
    ind = [random.randint(0,1) for _ in range(individual_size)]
    print(ind)
    set_bits_individual(ind,0,4,15)
    print(ind)
    exit(0)
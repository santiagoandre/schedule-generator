import array
import random
import json
def proccess_groups():
    with open("unprocessed_data/subjects_groups.json","r",encoding="iso-8859-14") as tsp_data:
        datos = json.load(tsp_data)


    with open("processed_data/teachers.json","r",encoding="iso-8859-14") as tsp_data:
        profesores = json.load(tsp_data)

    with open("processed_data/subjects.json","r",encoding="iso-8859-14") as tsp_data:
        subjects = json.load(tsp_data)


    labor= datos["Labor"]
    groups = []
    for i,grupo in enumerate(labor):
        subject = [subject for subject  in subjects  if subject['CODE']== grupo["CODIGO"]][0]
        group  = {}
        group["CODE"] = grupo["CODIGO"]
        group["CODE_GROUP"] = grupo["GRUPO"]
        group["CAPACITY"] = grupo["CUPO"]
        group["TEACHER"] = str(profesores.index(grupo["PROFESOR"]))
        group["BLOCKS"] =subject['BLOCKS']
        group["TYPE"] =subject['TYPE']
        group["SEMESTER"] =subject['SEMESTER']
        groups.append(group)
    print(len(groups), " groups")
    with open("processed_data/groups.json", 'w') as outfile:
        json.dump(groups,outfile,indent=4)
    return len(groups)
def proccess_classrooms():
    with open("unprocessed_data/classrooms.json","r",encoding="utf-8-sig") as tsp_data:
        salones = json.load(tsp_data)
#    salones = salones["Hoja1"]
    classrooms = {"T":[],"L":[],"L7":["245"]}
    for i,salon in enumerate(salones):
        classroom= {}
        type_c ="T" if salon["TIPO"][0] is "S" else "L"
        classrooms[type_c].append(salon["CODIGO"])
        
    print(len(classrooms), " classrooms")
    with open('processed_data/classrooms.json', 'w') as outfile:
        json.dump(classrooms,outfile,indent=4)
    return len(classrooms["T"]),len(classrooms["L"]),len(classrooms["L7"])
def proccess_subjects():
    with open("unprocessed_data/subjects_groups.json","r",encoding="iso-8859-14") as tsp_data:
        datos = json.load(tsp_data)

    labor= datos["Labor"]
    lastcode = -1
#    print(labor)
    j = 0
    subjects = []
    for i,grupo in enumerate(labor):
        subject = {}
        if  grupo["CODIGO"] == lastcode:
            continue
        else:
            subject["CODE"]= grupo["CODIGO"]
            subject["NAME"]= grupo["NOMBRE"]
            subject["SEMESTER"]= grupo["ASIGNATURA"]
            lastcode=grupo["CODIGO"]
            if "Laboratorio" in grupo["NOMBRE"]:
                subject["BLOCKS"] = 1
                if int(grupo["ASIGNATURA"]) >= 7:
                    subject["TYPE"] = "L7"
                else:
                    subject["TYPE"] = "L"
            else:
                subject["BLOCKS"] = 2
                subject["TYPE"] = "T"
            
            subjects.append(subject)
        
    print(len(subjects), " subjects")
    with open('processed_data/subjects.json', 'w') as outfile:
        json.dump(subjects,outfile,indent=4)
    return len(subjects)
        
 
def proccess_teachers():
    with open("unprocessed_data/subjects_groups.json","r",encoding="iso-8859-14") as tsp_data:
        datos = json.load(tsp_data)

    labor= datos["Labor"]
    lastname = -1
#    print(labor)
    j = 0
    teachers = []
    for i,grupo in enumerate(labor):

        if  grupo["PROFESOR"] == lastname or grupo["PROFESOR"] in teachers:
            continue
        else:
            lastname  =grupo["PROFESOR"]
            teachers.append(lastname)
        
    print(len(teachers), " teachers")
    with open('processed_data/teachers.json', 'w') as outfile:
        json.dump(teachers,outfile,indent=4)
    return len(teachers)

if __name__ == "__main__":
    jsons_size = {}
    jsons_size["NS"] = "56" # proccess_subjects() #56
    jsons_size["NT"] = str(proccess_teachers())
    jsons_size["NCT"],jsons_size["NCL"],jsons_size["NCL7"] = proccess_classrooms()
    jsons_size["NC"] = str(max(jsons_size["NCT"],jsons_size["NCL"],jsons_size["NCL7"]))
    jsons_size["NG"] = proccess_groups()
    with open('processed_data/jsons_size.json', 'w') as outfile:
        json.dump(jsons_size,outfile,indent=4)
    